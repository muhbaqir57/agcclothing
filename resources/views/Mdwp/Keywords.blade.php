@extends('adminlte::page')

@section('title', 'Keywords')

@section('content_header')
    <h1>Keywords</h1>
@stop

@section('content')
    <form action="{{ url('keywords/') }}" method="POST" name="main_keywords">
        @csrf
        @method("POST")
        <h5>Insert New Main Keyword</h5>
        <div class="col-md">
            <div class="form-group">
                <select name="categories">
                    <option value="" selected>--SELECT CATEGORIES--</option>
                    @foreach( $categories as $cat)
                        <option value="{{ $cat->id }}"> {{ $cat->categories_name }}</option>
                    @endforeach    
                </select>                
            </div>
        </div>
        <div class="col-md">
            <div class="form-group">
                <textarea name="main_keywords"></textarea>
            </div>    
            <button type="submit">Submit</button>
        </div>
    </form>
    <div class="col-md mt-5">
        <div class="table table-responsive">
            <table id="keywords" class="display" style="width: 100%" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Main Keyword</th>
                        <th>Derivative Keywords</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
    $(document).ready(function() {
        $('#keywords').DataTable();
    });
    </script>
@stop