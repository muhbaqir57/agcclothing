@extends('adminlte::page')

@section('title', 'Categories')

@section('content_header')
    <h1>Categories</h1>
@stop

@section('content')
    <form action="{{ url('categories/') }}" method="POST">
        @csrf
        @method("POST")
        <h5>Insert New Categories</h5>
        <div class="col-md">
            <div class="form-group">
                <input name="categories"></input>
            </div>    
            <button type="submit">Submit</button>
        </div>
    </form>

    <div class="col-md mt-5">
        <div class="table table-responsive">
            <table id="categories" class="display" style="width: 100%" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Categories</th>
                        <th>Keywords Count</th>
                        <th>Published Keywords</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
    $(document).ready(function() {
        $('#categories').DataTable();
    });
    </script>
@stop