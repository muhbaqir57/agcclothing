<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table    	=   "categories";
    protected $primaryKey   =   "id";

    protected $fillable = ['categories_name', 'categories_count'];


    public function images()
    {
        return $this->hasMany('App\Images', "categories_id");
    }
}
