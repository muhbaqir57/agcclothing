<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    

    protected $table        =   "tags";
    protected $primary_key  =   "id";
    protected $fillable     =   ['tags'];
}
