<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Images;
use App\Categories;

class CategoriesController extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories     = Categories::orderBy("categories_name")
                                ->take(10)
                                ->get();
    }

    public function index( $cat )
    {
        $categories     =   $this->categories;
        
        $most_viewed    =   Categories::where("categories_slug", "=", $cat)
    						->with(["images" => function($query){
                                $query->take(9);
                            }])
    						->get();

        $latest         =   Categories::where("categories_slug", "=", $cat)
                            ->leftJoin("images", "images.categories_id", "=", "categories.id" )
                            ->paginate(9);

        $page_title     =   $cat . ' - ' . config('global.title');

    	return view( "Main/categories" )->with( compact("most_viewed", "categories", "latest", "title") );
    }


}
