<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pages;
use App\Images;
use App\Categories;

class PagesController extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories     = Categories::orderBy("categories_name")
                                ->take(10)
                                ->get();
    }

    public function index( $pages )
    {	
        $categories     =   $this->categories;

        $page 			=	Pages::where( "page_slug", "=", $pages )->get();

        if( $page->isEmpty() )
        {
            abort(404);
        }

        $page_title     =   str_replace( "-", " ", $page[0]->page_title ) . " - " . config("global.title");

    	return view("Main/pages")->with( compact("page_title", "page", "categories") );
    }

    public function new_wallpaper()
    {
        $categories     =   $this->categories;

    	$latest         =   Images::orderBy("created_at", "desc")
                            ->paginate(12);

        $page_title     =   "Latest Wallpapers - " . config("global.title");

        return view("Main/latest")->with( compact("page_title", "latest", "categories") );
    }

    public function most_viewed()
    {
        $categories     =   $this->categories;

    	$most_viewed    =   Images::orderBy("view_count", "desc")
                            ->paginate(12);

        $page_title     =   "Most Viewed - " . config("global.title");

        return view("Main/most_viewed")->with( compact("page_title", "most_viewed", "categories") );
    }
}
