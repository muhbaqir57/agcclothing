<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Images;
use App\Categories;

class TagsController extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories     = Categories::orderBy("categories_name")
                                ->take(10)
                                ->get();
    }

    public function index()
    {
        abort( 404 );
    }

    public function tag( $tags )
    {
        $categories     =   $this->categories;
        $tag            =   Images::withAnyTag([ $tags ])->paginate(9);

        $page_title     =   $tags . ' - ' . config('global.title');

        return view("Main/tags")->with( 
                                        compact( "tag", "tags", "categories" )
                                    );
    }
}
