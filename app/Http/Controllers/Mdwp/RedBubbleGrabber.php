<?php 
namespace App\Http\Controllers\Mdwp;

use Symfony\Component\Panther\Client;
use Symfony\Component\DomCrawler\Crawler;
use PHPHtmlParser\Dom;
use __;

set_time_limit(0);
/**
* 
*/
class RedBubbleGrabber
{	
	public static function getValues($array)
	{
		$return = [];
		foreach ($array as $key => $value) {
			if(is_array($value)){
				foreach ($value as $vk => $vv) {
					$return[] = $vv;
				}
			}

			else{
				$return[] = $value;
			}
		}

		return $return;
	}

	public static function array_flatten($array) {

	   $return = array();
	   foreach ($array as $key => $value) {
	       if (is_array($value)){ $return = array_merge($return, self::array_flatten($value));}
	       else {$return[$key] = $value;}
	   }
	   return $return;
	}

	public static function filterResult($array, &$result)
	{
		$array = array_filter($array);

		foreach ($array as $key => $value) {
			$data = [];

			if(filter_var($value, FILTER_VALIDATE_URL)){
				$result[] = array_filter(self::array_flatten($array));
			}


			if(is_string($value)){
				$result[] = $value;
			}

			
			if(is_array($value)){
				self::filterResult($value, $result);
			}
		}
	}

	public static function grab($keyword, $options = [])
	{
		$results = [];

		for($i = 1; $i <= 2; $i++){
			$url = "https://www.redbubble.com/shop/?iaCode=u-tees&page=". $i ."&query=". urlencode( $keyword) ."&sleeveLength=sleeveLength-long-sleeve&sortOrder=relevant";

			$ua = \Campo\UserAgent::random([
			    'os_type' => ['Windows', 'OS X', 'Linux'],
			    'device_type' => 'Desktop'
			]);

			$options  = [
				'http' => [
					'method'     =>"GET",
					'user_agent' =>  $ua,
				],
				'ssl' => [
					"verify_peer"      => FALSE,
					"verify_peer_name" => FALSE,
				],
			];

			$client = Client::createFirefoxClient('../drivers/geckodriver.exe');
			$client->request("GET", $url);
			
			for( $j = 0; $j < 500; $j++ )
			{
				$client->executeScript("window.scrollBy(0,100);");
			}

			sleep(5);

			$crawler  	=	$client->refreshCrawler();

			$outputs  	= 	$crawler->filter('.styles__productImage--3ZNPD')
							->extract(['src', 'class']);

			foreach ($outputs as $output) {
				$result = [];
				$result['filetype']	=	self::getFileType($output[0]);
				$result['url'] = $output[0];
				$results[] = $result;
			}

			$client->quit();
		}

		try {
	        exec("taskkill /F /IM geckodriver.exe");
	        exec("taskkill /F /IM plugin-container.exe");
	        exec("taskkill /F /IM firefox.exe");
	    } catch (IOException $e) {
	        $e.printStackTrace();
	    }

		return $results;
	}

	public static function getFileType($url)
	{
		$url = strtolower($url);

		switch ($url) {
			case strpos($url, '.jpg') || strpos($url, '.jpeg'):
				return 'jpg';
				break;

			case strpos($url, '.png'):
				return 'png';
				break;

			case strpos($url, '.bmp'):
				return 'bmp';
				break;

			case strpos($url, '.gif'):
				return 'gif';
				break;
			
			default:
				return 'png';
				break;
		}
	}
}
