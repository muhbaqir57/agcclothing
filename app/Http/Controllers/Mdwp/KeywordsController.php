<?php

namespace App\Http\Controllers\Mdwp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Buchin\GoogleImageGrabber\GoogleImageGrabber as GIG;
use App\Keywords;
use App\derived_keywords;

class KeywordsController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories     =   \App\Categories::all();

        return view( "Mdwp/Keywords")->with( compact( "categories" ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $keywords   =   preg_split('/\r\n|[\r\n]/', $request->main_keywords );
        
        $key    =   [];
        // Get keyword suggestion from main keywords
        foreach ($keywords as $value) {
            $key['main_keywords']   =   strtolower($value);
            $key['categories_id']   =   $request->categories;
            $key['status']          =   false;
            
            $keyword        =   Keywords::updateOrCreate(['main_keywords' => $value], $key);
        }        

        return response()->json( "success" );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_datatables( Request $request )
    {

    }
}
