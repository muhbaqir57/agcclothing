<?php

namespace App\Http\Controllers\Mdwp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Mdwp\RedBubbleGrabber as RBG;

use App\Keywords;
use App\derived_keywords;
use App\Images;

use Storage;

use Image as Resizer;

set_time_limit(0);

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $keywords   =   Keywords::join(
                            'categories', 'categories.id', '=', 'keywords.categories_id'
                        )->where('status', FALSE)
                        ->get();

        $data   =   [];
        $ua     = \Campo\UserAgent::random([
                    'os_type' => ['Windows', 'OS X', 'Linux'],
                    'device_type' => 'Desktop'
                ]);

        $options  = [
            'http' => [
                'method'     =>"GET",
                'user_agent' =>  $ua,
            ],
            'ssl' => [
                "verify_peer"      => FALSE,
                "verify_peer_name" => FALSE,
            ],
        ];

        foreach ($keywords as $val_0) {
            try {
                $data   =   RBG::grab($val_0->main_keywords);
                foreach ($data as $key => $val_1) {
                    // Download image by derived keywords
                    $context    =   stream_context_create($options);

                    if( $val_1['url'] !== "/boom/client/13560495740213f434dea5756e8543bc.svg" ){
                        $image      =   file_get_contents( $val_1['url'], FALSE, $context );
                    }
                    
                    $categories =   $val_0->categories_slug;  
                    $keyword    =   str_replace(" ", "-", $val_0->main_keywords);
                    $name       =   str_replace(" ", "-", $val_0->main_keywords) . "-" . uniqid(). '.' . $val_1['filetype'];
                    $dir        =   Storage::disk('public')->makeDirectory( $categories . "/". $keyword );
                    $image_path =   $categories . "/" . $keyword . "/" . $name;

                    // Save to storage
                    if( $image != "/boom/client/13560495740213f434dea5756e8543bc.svg")
                    {
                        Storage::disk('public')->put( $image_path, $image );
                        // Insert path to table Images
                        $img    =   new Images;

                        $img->image_title   =   $keyword;
                        $img->image_path    =   $image_path;
                        $img->image_thumb   =   "";
                        $img->categories_id =   $val_0->categories_id;
                        $img->download_count=   0;
                        $img->view_count    =   0;
                        $img->slug          =   \Str::slug( $img->image_title );

                        $img->save();
                    }
                    
                    // Update keywords status
                    Keywords::where('id', $val_0->id)->update(['status' => TRUE ]);
                }
                
            } catch (\Throwable $e) {
                echo "<pre>";
                return $e;
            }
        }

        return view("Mdwp/Post");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generate()
    {

    }

    public function resize_image($image, $keyword, $filetype, $img_id)
    {
        $dir    =   Storage::disk('public')->makeDirectory( "thumbnails/" . $keyword . "/". $img_id );
        $str_path   =   "storage/thumbnails/" . $keyword . "/". $img_id . "/350x220." . $filetype;
        $path       =   public_path("storage/thumbnails/" . $keyword . "/". $img_id . "/350x220." . $filetype);

        $canvas =   Resizer::canvas( 350, 220 );
        $img    =   Resizer::make( $image )->resize( null, 220, function( $constraint ){
                                                                    $constraint->aspectRatio();
                                                                });

        $canvas->insert( $img, 'center' );
        $canvas->save( $path, 70);

        return $str_path;        
    }

    private function remove_duplicate( $str )
    {
        $str   =   implode(',', array_unique( explode(',', $str) ));

        return $str;
    }
}
