<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keywords extends Model
{
    protected $table        =   "keywords";
    protected $primary_key  =   "id";
    protected $fillable 	=	['main_keywords', 'categories_id', 'status'];


    public function derived_keywords()
    {
        return $this->hasMany('App\derived_keywords');
    }
}
