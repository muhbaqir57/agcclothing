<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("/",     "Mdwp\HomeController@index");
Route::get("/home", "Mdwp\HomeController@index");

// Route for post
Route::resource("/post",     "Mdwp\PostController");
Route::get("/post/generate", "Mdwp\PostController@generate");

// Route for keywords
Route::resource("/keywords", "Mdwp\KeywordsController");

// Route for categories
Route::resource("/categories",  "Mdwp\CategoriesController");

//Route for pages
Route::resource("/pages",  "Mdwp\PagesController");
